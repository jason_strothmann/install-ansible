package main

import (
	"flag"
	"log"
	"os"
	"os/exec"
	"os/user"
	"path/filepath"
	"strconv"
	"strings"
)

const (
	version       = "1.0.0"
	defAnsibleTag = "v1.9.0-2"
)

var (
	packages = []string{
		"git",
		"wget",
		"python",
		"python-setuptools",
		"python-crypto",
	}
	ansibleTag = flag.String("t", "", "git tag to clone")
	userName   = flag.String("u", "", "user account to install ansible into")
	userAcc    *user.User
)

func init() {
	log.SetFlags(0)
}

func main() {
	ensureElevatedPrivileges()
	log.Printf("Version: %s", version)
	parseFlags()
	updateEnvironmentalVariables()
	log.Print("Added environmental variable to .profile")
	installPackages()
	log.Print("Added apt packages")
	cloneAnsible()
	log.Print("Cloned ansible package")
	installPipPackages()
	log.Print("Installed pip packages")
}

func installPipPackages() {
	var c *exec.Cmd
	c = exec.Command("easy_install", "pip")
	if err := c.Run(); err != nil {
		log.Fatalf("Error: Could not install pip: %s", err.Error())
	}
	c = exec.Command("pip", "--isolated", "install", "paramiko", "PyYAML", "Jinja2", "httplib2")
	if err := c.Run(); err != nil {
		log.Fatalf("Error: Could not install pip packages: %s", err.Error())
	}
}

func cloneAnsible() {
	var c *exec.Cmd
	c = exec.Command("git", "clone", "git://github.com/ansible/ansible.git",
		"--recursive", "-b", *ansibleTag, filepath.Join(userAcc.HomeDir, "ansible"))
	if err := c.Run(); err != nil {
		log.Fatalf("Error: Could not clone ansible: %s", err.Error())
	}
	c = exec.Command("chown", "-R", strings.Join([]string{*userName, ":", *userName}, ""), filepath.Join(userAcc.HomeDir, "ansible"))
	if err := c.Run(); err != nil {
		log.Fatalf("Error: Could not change directory ownership: %s", err.Error())
	}
}

func installPackages() {
	var c *exec.Cmd
	c = exec.Command("apt", "update")
	if err := c.Run(); err != nil {
		log.Fatalf("Error: Could not update apt: %s", err.Error())
	}
	for i := 0; i < len(packages); i++ {
		c = exec.Command("apt", "install", "-y", packages[i])
		if err := c.Run(); err != nil {
			log.Fatalf("Error: Could not install package: %s", err.Error())
		}
	}
}

func parseFlags() {
	flag.Parse()
	if *ansibleTag == "" {
		*ansibleTag = defAnsibleTag
	}
}

func updateEnvironmentalVariables() {
	var (
		err error
		fi  os.FileInfo
		f   *os.File
	)

	if userAcc, err = user.Lookup(*userName); err != nil {
		log.Fatal("Error: Could not retrieve specified user account.")
	}

	if fi, err = os.Stat(userAcc.HomeDir); err != nil {
		log.Fatalf("Error: Could not get user's home directory: %s", err.Error())
	}
	if !fi.IsDir() {
		log.Fatalf("Error: Could not get user's home directory: %s", userAcc.HomeDir)
	}
	if f, err = os.OpenFile(filepath.Join(userAcc.HomeDir, ".profile"), os.O_RDWR|os.O_APPEND, 0666); err != nil {
		log.Fatalf("Error: Could not open .profile file: %s", err.Error())
	} else {
		if _, err = f.WriteString("source ~/ansible/hacking/env-setup -q\n"); err != nil {
			log.Fatalf("Error: Could not write to .profile: %s", err.Error())
		}
		f.Close()
	}
}

func ensureElevatedPrivileges() {
	if curUser, err := user.Current(); err == nil {
		if userID, err := strconv.ParseInt(curUser.Uid, 10, 64); err == nil && userID == 0 {
			return
		}
	}
	log.Fatal("Error: Elevated privileges are required to install packages.")
}
